import axios from 'axios'
import router from './../router/index.js'
import eventBus from '@/eventBus'

export function baseWithToken (obj, successCallBack, errorCallBack) {
  obj.data = obj.data ? obj.data : {}
  const TOKEN = sessionStorage.getItem("token")
  const USERNAME = sessionStorage.getItem("username")
  obj.data.username = USERNAME
  obj.data.token = TOKEN
  const config = {
    headers: {
      'Authorization': 'Bearer ' + TOKEN,
      'Token': TOKEN
    }
  }
  axios({
    method: obj.method ? obj.method : 'post',
    url: obj.url,
    data: obj.data ? obj.data : undefined,
    config
  })
  .then(response => {
    if (response.status === 200 && response.data.status === 'success') {
      successCallBack(response)
    } else {
      errorCallBack('Error on sending')
    }
  })
  .catch(error => {
    if (error.response && error.response.status === 403) {
      router.push({ path: '/' })
      eventBus.$emit('logout')
    } else {
      errorCallBack(error)
    }
  })
}
