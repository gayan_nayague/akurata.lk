export default {
    async get(url, sucessCallback, errorCallback, token) {
        await fetch(url, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        })
        .then((response) => {
            if (!response.ok) {
                throw response
            }
            return response.json()})

        .then( json => {
            sucessCallback(json)
        })
        .catch(err => {
            errorCallback(err)
        })
    },

    async tokenPost(url, sucessCallback, errorCallback, data) {
        await fetch(url, {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then((response) => {
            if (!response.ok) {
                throw response
            }
            return response.json()
        })
        .then( json => {
            sucessCallback(json)
        })
        .catch(err => {
            errorCallback(err)
        })
    },

    async post(url, sucessCallback, errorCallback, data) {
        await fetch(url, {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then((response) => {
            if (!response.ok) {
                throw response
            }
            return response.json()})

        .then( json => {
            sucessCallback(json)
        })
        .catch(err => {
            errorCallback(err)
        })
    }
}