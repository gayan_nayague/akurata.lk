import baseAPIExicuter from './baseAPIExicuter'

export default {
    async getUserLoggedIn (userName, password, sucessCallback, errorCallback) {
        var obj = {
            username: userName,
            password: password,
            rememberMe: false
        }
        var url = 'https://qa.akurata.lk/web-api/api/login'
        return await baseAPIExicuter.post(url, sucessCallback, errorCallback, obj)
    },

    async passwordReset (email, sucessCallback, errorCallback) {
        var obj = {
            email: email,
        }
        var url = 'https://qa.akurata.lk/web-api/api/reset-password'
        return await baseAPIExicuter.post(url, sucessCallback, errorCallback, obj)
    },

    async getUserDetails (userName, token, sucessCallback, errorCallback) {
        var obj = {
            username: userName,
            token: token
        }
        var url = 'https://qa.akurata.lk/web-api/api/get-account'
        return await baseAPIExicuter.tokenPost(url, sucessCallback, errorCallback, obj)
    }
}