import * as React from 'react';
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default function Header({icon1, icon2, color1, color2, userImageUrl, onPress1, onPress2 }) {
    return (
        <View style={styles.topIconContainer}>

            <TouchableOpacity style={{width:'10%'}} onPress={onPress1}>
                <Ionicons name={icon1} size={30} color={color1} />
            </TouchableOpacity>

            <View style={styles.welcomeContainer}>
                <Image
                    source={{uri:userImageUrl}}
                    style={styles.welcomeImage}
                />
            </View>

            <TouchableOpacity  onPress={onPress2}>
                <Ionicons name={icon2} size={30} color={color2} />
            </TouchableOpacity>

      </View>
    );
}

const styles = StyleSheet.create({
    topIconContainer: {
        alignSelf:'center',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop:5
    },
    welcomeContainer: {
        alignItems: 'center',
        marginBottom: 5,
        width:'80%'
    },
    welcomeImage: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginTop: 3,
        alignSelf:'center',
        borderRadius: 15
    },
});