import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import PasswordResetScreen from '../screens/PasswordResetScreen';
import CheckLoginState from './CheckLoginState';

const MainStack = createStackNavigator();

export default Main = () => {

    const logged = CheckLoginState.checkLogin()
    if(logged){
        return (
            <MainStack.Navigator>
                <MainStack.Screen options={{headerShown: false}} name="Home" initialParams={{ name:'HomeScreen'}} component={HomeScreen} />
                <MainStack.Screen options={{headerShown: false}} name="Login" component={LoginScreen} />
                <MainStack.Screen options={{headerShown: false}} name="ResetPassword" component={PasswordResetScreen} />
            </MainStack.Navigator>
        );
    }
    else{
        return (
            <MainStack.Navigator>
                <MainStack.Screen options={{headerShown: false}} name="Login" component={LoginScreen} />
                <MainStack.Screen options={{headerShown: false}} name="Home" component={HomeScreen} />
                <MainStack.Screen options={{headerShown: false}} name="ResetPassword" component={PasswordResetScreen} />
            </MainStack.Navigator>
        );
    }
}