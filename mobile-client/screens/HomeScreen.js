import * as React from 'react';
import { Image, StyleSheet, Text, View, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import BottomNavigationBar from '../components/BottomNavigationBar';
import { TextInput } from 'react-native-gesture-handler';
import Header from '../components/Header';
import homeAPI from '../APIExicuters/homeAPI';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState, useEffect } from 'react';
import CommonButton from '../components/CommonButton';

export default function HomeScreen (props) {

  const [userToken, setUserToken] = useState('');
  const [userDetails, setUserDetails] = useState({});
  const [userClasses, setUserClasses] = useState([]);

  useEffect(() => {
    getToken()
  }, []);

  var spinner = false
  const userName = 'Mike'
  const userImageUrl = 'https://image.flaticon.com/icons/png/512/190/190634.png'
  const dummyData = [{promotionImage:'https://www.dlf.pt/png/big/11/117461_xbox-gift-card-png.png', moredetailUrl:'https://www.xbox.com/en-GB/promotions/sales/sales-and-specials', clientName:'XBOX', clientId:0, clientLogo: 'https://pluspng.com/img-png/xbox-png-xbox-logo-png-1220.png', promotionDetails: '10% off for your next bill', value: 1000, currencyType:'Rs' },
  {promotionImage:'http://www.pngmart.com/files/10/Amazon-Gift-Card-Transparent-Background.png', moredetailUrl:'https://www.amazon.com/gift-cards/b/?ie=UTF8&node=2238192011&ref_=nav_cs_gc', clientName:'amazon', clientId:1, clientLogo: 'https://pngimg.com/uploads/amazon/amazon_PNG5.png', promotionDetails: 'Buy one get one free',value: 2000, currencyType:'Rs'},
  {promotionImage:'https://www.pngkey.com/png/full/0-5222_google-play-gift-cards-google-play-gift-card.png', moredetailUrl:'https://play.google.com/store/movies?hl=en', clientName:'Google Play', clientId:3, clientLogo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Google_Play_symbol.svg/1200px-Google_Play_symbol.svg.png', promotionDetails: 'Special offer',value: 1500, currencyType:'Rs'}]

  // function renderClassData (item) {
  //   return(
  //     <View style={styles.promotionDataCard} >
  //       <Image
  //         source={{uri:item.clientLogo}}
  //         style={styles.clientLogo}
  //       />
  //       <Image
  //         source={{uri:item.promotionImage}}
  //         style={styles.promotionImage}
  //       />
  //     </View>
  //   )    
  // }

  function renderClassData (item) {
    return(
      <View style={styles.classDataCard}>
        <View style={styles.cardButtonContainer}>
          <CommonButton
            icon={'ios-log-in'}
            iconColor={'black'}
            iconSize={15}
            buttonColor={'transparent'}
            buttonText={'Approved'}
            width={90}
            height={40}
            borderColor={'black'}
            fontSize={15}
            onPress={()=>{updateCount()}}
          />
          <CommonButton
            icon={'ios-log-in'}
            iconColor={'black'}
            iconSize={15}
            buttonColor={'transparent'}
            buttonText={'Pending'}
            width={90}
            height={40}
            borderColor={'black'}
            fontSize={15}
            onPress={()=>{updateCount()}}
          />
          <CommonButton
            icon={'ios-log-in'}
            iconColor={'black'}
            iconSize={15}
            buttonColor={'transparent'}
            buttonText={'Rejected'}
            width={90}
            height={40}
            borderColor={'black'}
            fontSize={15}
            onPress={()=>{updateCount()}}
          />
        </View>
        <Text>{item.assignee_username}</Text>
      </View>
    )    
  }

  const getToken = async () => {
    spinner = true
    try {
      const value = await AsyncStorage.getItem('@token');
      if (value !== null) {
        setUserToken(value)
        getUser()
      }
    } catch (error) {
    }
  };

  const getUser = async () => {
    try {
      const value = await AsyncStorage.getItem('@userDetails');
      if (value !== null) {
        var user = JSON.parse(value)
        setUserDetails(user)
        getUserEnrolledClasses()
      }
    } catch (error) {
    }
  };

  function getUserEnrolledClasses(){
    var offSet = 0
    var perPage = 10
    homeAPI.getUserClasses(userDetails.username, userToken, userDetails.id, offSet, perPage, function(response){
      setUserClasses(response.classes)
      console.log(userClasses)
      spinner = false
    },
    function(error){
      console.log('error',error)
      spinner = false
    })
  }

  function updateCount() {

  }

  return (
    <View style={styles.container}>
      <StatusBar hidden={true} />
      <Header 
        icon1={'md-menu'}
        icon2={'md-cart'}
        color1={'black'}
        color2={'black'}
        userImageUrl={userImageUrl}
        onPress2={()=>{updateCount()}}
      />
      <View style={styles.searchBarContainer}> 
        <TextInput style={styles.searchBar} placeholder="🔍 Classes"></TextInput>
      </View>

      <View style={styles.flatListContainer}>
        <FlatList
          data={userClasses}
          renderItem={({item}) =>renderClassData(item)}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
      <BottomNavigationBar
        icon1="md-home"
        icon2="md-man"
        icon3="md-wallet"
        icon4="md-mail-unread"
        onPress2={() => {updateCount()}}
        onPress3={() => {updateCount()}}       
        onPress4={() => {updateCount()}}
        color1={"black"}
        color2={"rgba(0,0,0,0.20)"}
        color3={"rgba(0,0,0,0.20)"}
        color4={"rgba(0,0,0,0.20)"}
        text1={'Home'}
        text2={'Teachers'}
        text3={'Payments'}        
        text4={'Notification'}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  cardButtonContainer: {
    alignItems: 'center',
    alignSelf:'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  flatListContainer: {
    flex: 1,
    backgroundColor: '#fff',
    marginBottom: 55
  },
  classDataCard: {
    flex: 1,
    height: 200,
    width:'100%',
    backgroundColor: 'red',
    borderColor: '#ffffff',
    borderRadius:20,
    borderWidth: 3,
    alignItems: 'center',
    alignSelf:'center',
    flexDirection: 'column',
    justifyContent: 'space-evenly'
  },
  promotionImage: {
    height: '100%',
    width: '70%',
    marginRight:20
  },
  clientLogo: {
    height:70,
    width:70,
    resizeMode: 'contain',
    borderRadius: 35
  },
  searchBar: {
    marginLeft:10,
    marginRight:10,
    height:20,
  },
  searchBarContainer: {
    marginLeft:40,
    marginRight:40,
    height:20,
    borderRadius: 5,
    marginBottom:2,
    backgroundColor:'#dedede'
  }
});
