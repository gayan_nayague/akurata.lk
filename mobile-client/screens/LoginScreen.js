import * as React from 'react';
import { StyleSheet, View, Image, StatusBar, TextInput, Text} from 'react-native';
import { useState } from 'react';
import CommonButton from '../components/CommonButton';
import loginAPI from '../APIExicuters/loginAPI';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function LoginScreen (props) {

    const [userName, setUserName] = useState('');
    const [userPassword, setUserPassword] = useState('');

    function getLoginDetails(){
        var token = null
        loginAPI.getUserLoggedIn(userName, userPassword, function(response){
            if (response != null) {
                token = response.token
                storeToken(token)
                loginAPI.getUserDetails(userName, token, function(response){
                    if (response != null) {
                        storeUserDetails(response.data[0])
                        props.navigation.navigate('Home')
                    }
                },
                function(error){
                    console.log(error)  
                })
            }
        },
        function(error){
            console.log(error)
        })
    }

    const storeToken = async (value) => {
        try {
            await AsyncStorage.setItem('@token', value)
        } catch (e) {
            console.log(e)
        }
    }

    const storeUserDetails = async (value) => {
        try {
            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('@userDetails', jsonValue)
        } catch (e) {
            console.log(e)
        }
    }

    return (
        <View style={styles.container}>
            <StatusBar hidden={true} />

            {/* <Image source={Logo} style={styles.LogoImageContainer}></Image> */}
            <View style={styles.headerTextContainer}>
                <Text style={styles.headerText}>Welcome Akurata</Text>
            </View>

            <View style={styles.inputContainer}>
                <View style={styles.textInput}>
                    <TextInput style={styles.userInput}
                        placeholder=" Type User Name "
                        onChangeText={text => setUserName(text)}
                        value={userName}
                    >                    
                    </TextInput>
                    <TextInput style={styles.userInput}
                        placeholder=" Type Password "
                        onChangeText={text => setUserPassword(text)}
                        value={userPassword}
                    >
                    </TextInput>
                </View>

                <View style={styles.loginButton}>
                <CommonButton
                    icon={'ios-log-in'}
                    iconColor={'black'}
                    iconSize={30}
                    buttonColor={'transparent'}
                    buttonText={'Login'}
                    width={150}
                    height={40}
                    borderColor={'black'}
                    fontSize={25}
                    onPress={()=>{getLoginDetails()}}
                />
                <CommonButton
                    icon={'ios-log-in'}
                    iconColor={'black'}
                    iconSize={30}
                    buttonColor={'transparent'}
                    buttonText={'Reset'}
                    width={150}
                    height={40}
                    borderColor={'black'}
                    fontSize={25}
                    onPress={()=>{props.navigation.navigate('ResetPassword')}}
                />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems:'center'
    },
    LogoImageContainer: {
        height:100, 
        width:100, 
        alignSelf:'center', 
        resizeMode:'contain'
    },
    inputContainer: {
        flex:1,
        alignItems: 'center',
        flexDirection: 'column',
    },
    userInput: {
        width:300,
        height:30,
        borderBottomColor: '#9e9e9e',
        borderBottomWidth: 2,
        //margin:30
        //backgroundColor:'#dedede',
        //borderRadius:5
    },
    textInput:{
        alignItems: 'center',
        justifyContent:'space-evenly',
        flex:2
    },
    loginButton:{
        flex:1
    },
    headerTextContainer:{
        marginTop: 50,
    },
    headerText:{
        color: 'black',
        fontSize: 40
    }
});
