import * as React from 'react';
import { StyleSheet, View, Image, StatusBar, TextInput, Text} from 'react-native';
import { useState } from 'react';
import CommonButton from '../components/CommonButton';
import loginAPI from '../APIExicuters/loginAPI';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function PasswordResetScreen (props) {

    const [userEmail, setUserEmail] = useState('');

    function resetPassword(){
        var reset = null
        loginAPI.passwordReset(userEmail, function(response){
            if (response != null) {
              props.navigation.navigate('Home')
            }
        },
        function(error){
            console.log(error)
        })
    }

    return (
        <View style={styles.container}>
            <StatusBar hidden={true} />

            {/* <Image source={Logo} style={styles.LogoImageContainer}></Image> */}
            <View style={styles.headerTextContainer}>
                <Text style={styles.headerText}>reset</Text>
            </View>

            <View style={styles.inputContainer}>
                <View style={styles.textInput}>
                    <TextInput style={styles.userInput}
                        placeholder=" Type User Email "
                        onChangeText={text => setUserEmail(text)}
                        value={userEmail}
                    >                    
                    </TextInput>
                </View>

                <View style={styles.loginButton}>
                <CommonButton
                    icon={'ios-log-in'}
                    iconColor={'black'}
                    iconSize={30}
                    buttonColor={'transparent'}
                    buttonText={'Reset'}
                    width={150}
                    height={40}
                    borderColor={'black'}
                    fontSize={25}
                    onPress={()=>{resetPassword()}}
                />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems:'center'
    },
    LogoImageContainer: {
        height:100, 
        width:100, 
        alignSelf:'center', 
        resizeMode:'contain'
    },
    inputContainer: {
        flex:1,
        alignItems: 'center',
        flexDirection: 'column',
    },
    userInput: {
        width:300,
        height:30,
        borderBottomColor: '#9e9e9e',
        borderBottomWidth: 2,
        //margin:30
        //backgroundColor:'#dedede',
        //borderRadius:5
    },
    textInput:{
        alignItems: 'center',
        justifyContent:'space-evenly',
        flex:2
    },
    loginButton:{
        flex:1
    },
    headerTextContainer:{
        marginTop: 50,
    },
    headerText:{
        color: 'black',
        fontSize: 40
    }
});
